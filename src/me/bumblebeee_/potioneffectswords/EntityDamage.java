package me.bumblebeee_.potioneffectswords;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class EntityDamage implements Listener {
	
	Plugin pl = null;
	public EntityDamage(Plugin plugin) {
		pl = plugin;
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if (p.getInventory().getItemInMainHand() != null) {
				if (p.getInventory().getItemInMainHand().hasItemMeta()) {
					
					String name = p.getInventory().getItemInMainHand().getItemMeta().getDisplayName().replaceAll("§", "&");
					if (PotionEffectSwords.en.containsKey(name)) {
						if (e.getDamager().hasPermission("effects.use")) {
							String t = PotionEffectSwords.en.get(name);
							String effect = PotionEffectSwords.config.getString(t + ".effect").toUpperCase();
							int level = pl.getConfig().getInt(t + ".level") - 1;
							int duration = pl.getConfig().getInt(t + ".duration");
							if (e.getEntity() instanceof LivingEntity) {
								((LivingEntity) e.getEntity()).addPotionEffect(new PotionEffect(PotionEffectType.getByName(effect), duration * 20, level));
							}
						}
					}
				}
			}
		}
	}
}








