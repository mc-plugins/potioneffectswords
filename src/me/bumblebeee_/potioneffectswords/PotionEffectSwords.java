package me.bumblebeee_.potioneffectswords;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class PotionEffectSwords extends JavaPlugin {
				//DisplayName, Actual Name
	static HashMap<String, String> en = new HashMap<String, String>();
	public static Configuration config = null;
	
	public void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new EntityDamage(this), this);
		saveDefaultConfig();
		config = getConfig();
		
		for (String s : getConfig().getStringList("enabled")) {
			en.put(getConfig().getString(s + ".displayName"), s);
		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("pes")) {
			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("reload")) {
					reloadConfig();
					sender.sendMessage(ChatColor.GREEN + "Successfully reloaded config!");
				} else {
					sender.sendMessage(ChatColor.RED + "Invalid arguments. Correct usage: /pes reload");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "Invalid arguments. Correct usage: /pes reload");
				return true;
			}
		}
		
		
		return true;
	}

}
